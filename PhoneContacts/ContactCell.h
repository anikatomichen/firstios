//
//  ContactCell.h
//  PhoneContacts
//
//  Created by anika on 05/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *contactImage;
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UILabel *contactNumber;


@end
