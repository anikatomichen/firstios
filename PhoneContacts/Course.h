//
//  Course.h
//  PhoneContacts
//
//  Created by anika on 05/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Course : UIViewController

@property (strong, nonatomic)NSString *name;
@property (strong, nonatomic)NSNumber *number;
@property (strong, nonatomic)UIImage *image;
@property (strong, nonatomic) NSString * emailAddresss;
@property (strong, nonatomic) NSString * company;

@end
