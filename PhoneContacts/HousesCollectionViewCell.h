//
//  HousesCollectionViewCell.h
//  PhoneContacts
//
//  Created by anika on 13/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HousesCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *houseImage;
@property (weak, nonatomic) IBOutlet UILabel *houseName;

@end
