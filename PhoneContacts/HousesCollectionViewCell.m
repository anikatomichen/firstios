//
//  HousesCollectionViewCell.m
//  PhoneContacts
//
//  Created by anika on 13/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import "HousesCollectionViewCell.h"

@implementation HousesCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _houseImage.contentMode=UIViewContentModeScaleAspectFill;
}

@end
