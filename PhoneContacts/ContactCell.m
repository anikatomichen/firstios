//
//  ContactCell.m
//  PhoneContacts
//
//  Created by anika on 05/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
