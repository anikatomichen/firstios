//
//  SingleContactDetails.m
//  PhoneContacts
//
//  Created by anika on 07/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import "SingleContactDetails.h"
#import "ContactListingViewController.h"
#import "HousesCollectionViewCell.h"
#import "HouseDetails.h"

@interface SingleContactDetails ()<contactListingDeligate>
@property (strong, nonatomic) NSMutableArray <HouseDetails *>* houseArray;
@end

@implementation SingleContactDetails



- (IBAction)backToPrevious:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.housesCollectionView registerNib:[UINib nibWithNibName:@"HousesCollectionViewCell" bundle:nil]
        forCellWithReuseIdentifier:@"houseCellID"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    ContactListingViewController *contactListingViewController=[[ContactListingViewController alloc]init];
    contactListingViewController.deligate = self;
    [self setUpHouses];
}

-(void)setUpHouses {
    self.houseArray = NSMutableArray.new;
    HouseDetails *houseDetails1 = [[HouseDetails alloc]init];
    houseDetails1.nameOfHouse = @"gryffindor";
    houseDetails1.houseLogo = [UIImage imageNamed:@"images1.jpg"];
    [self.houseArray addObject:houseDetails1];
    
    
    HouseDetails *houseDetails2 = [[HouseDetails alloc]init];
    houseDetails2.nameOfHouse = @"Slytherin";
    houseDetails2.houseLogo = [UIImage imageNamed:@"image3.jpg"];
    [self.houseArray addObject:houseDetails2];
    
    
    HouseDetails *houseDetails3 = [[HouseDetails alloc]init];
    houseDetails3.nameOfHouse = @"Ravenclaw";
    houseDetails3.houseLogo = [UIImage imageNamed:@"images2.jpg"];
    [self.houseArray addObject:houseDetails3];
    
    HouseDetails *houseDetails4= [[HouseDetails alloc]init];
    houseDetails4.nameOfHouse = @"hufflepuff";
    houseDetails4.houseLogo = [UIImage imageNamed:@"image3.jpg"];
    [self.houseArray addObject:houseDetails4];
    
}

- (CGSize)collectionView:(UICollectionView* )collectionView layout:(UICollectionViewLayout* )collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((collectionView.frame.size.width/4)-5, (collectionView.frame.size.width/4)-5);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return self.houseArray.count;
    
}

-(UICollectionViewCell*) collectionView:(UICollectionView *) collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    HousesCollectionViewCell *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"houseCellID" forIndexPath:indexPath];
    HouseDetails *schoolHouseDetails = self.houseArray[indexPath.row];
    myCell.houseName.text = schoolHouseDetails.nameOfHouse;
    myCell.houseImage.image = schoolHouseDetails.houseLogo;
   // myCell.houseImage.image.contentMode=UIViewContentModeScaleAspectFit;
    return myCell;
    
}

-(void)passContactDetails:(NSString *)company andEmail:(NSString *)email andImage:(UIImage *)contactImage
{
    
    self.companyName.text = company;
    self.email.text = email;
    self.contactImage.image = contactImage;
    
}

-(void)collectionView: (UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    
    HouseDetails *schoolHouseDetails = self.houseArray[indexPath.row];
    self.chosenHouse.text = schoolHouseDetails.nameOfHouse;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}





@end
