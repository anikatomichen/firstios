//
//  SingleContactDetails.h
//  PhoneContacts
//
//  Created by anika on 07/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleContactDetails : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *email;
@property (weak, nonatomic) IBOutlet UIImageView *contactImage;
@property (weak, nonatomic) IBOutlet UICollectionView *housesCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *chosenHouse;


-(void)passContactDetails:(NSString *)company andEmail:(NSString *)email andImage:(UIImage *)contactImage;
@end
