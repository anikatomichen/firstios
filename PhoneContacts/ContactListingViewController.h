//
//  ContactListingViewController.h
//  PhoneContacts
//
//  Created by anika on 07/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol contactListingDeligate<NSObject>
@required
-(void)passContactDetails:(NSString *)company andEmail:(NSString *)email andImage:(UIImage *)contactImage;
@end

@interface ContactListingViewController : UIViewController
@property (strong, nonatomic) id <contactListingDeligate> deligate;
@end
