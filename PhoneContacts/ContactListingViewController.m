//
//  ContactListingViewController.m
//  PhoneContacts
//
//  Created by anika on 07/09/18.
//  Copyright © 2018 anika. All rights reserved.
//

#import "ContactListingViewController.h"
#import "Course.h"
#import "ContactCell.h"
#import "SingleContactDetails.h"

@interface ContactListingViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableOfContacts;
@property (strong, nonatomic)NSMutableArray<Course *> *contactDetails;

@end

@implementation ContactListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpCourses];
  
   
}

-(void)setUpCourses{
    self.contactDetails=NSMutableArray.new;  //object of the mutable array
    Course *course1=[[Course alloc] init];  //object of the class
    course1.name=@"Doraemon";  //set value to property name
    course1.number=@(49);       //set value to property no. of lessons
    course1.image=[UIImage imageNamed:@"Doraemon.png"];
    course1.emailAddresss=@"doraemon@hungama.com";
    course1.company=@"Hungama";
    [self.contactDetails addObject:course1];
    
    Course *course2=[[Course alloc]init];
    course2.name=@"Kitretsu";
    course2.number=@(30);
    course2.image=[UIImage imageNamed:@"Unknown.jpeg"];
    course2.emailAddresss=@"kitretsu@student.com";
    course2.company=@"Disney";
    [self.contactDetails addObject:course2];      //th eobject of htese properties sent to the mutable array Object. array of objects.
    
    Course *course3=[[Course alloc]init];
    course3.name=@"Shinchan";
    course3.number=@(40);
    course3.image=[UIImage imageNamed:@"Unknown-1.jpeg"];
    course3.emailAddresss=@"shinchan@gmail.com";
    course3.company=@"POGO";
    [self.contactDetails addObject:course3];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.contactDetails.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ContactCell *cell=(ContactCell *)[tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    if(cell==nil){
        
        NSArray *cellArray=[[NSBundle mainBundle]loadNibNamed:@"ContactCell" owner:self options:nil];
        cell=[cellArray firstObject];
        
    }
    
    Course *course= self.contactDetails[indexPath.row];
    cell.contactNumber.text = [NSString stringWithFormat:@"%@",course.number];
    cell.contactImage.image = course.image;
    cell.contactName.text=course.name;
    return cell;
}


-(void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    SingleContactDetails *singleContactDetails = [[SingleContactDetails alloc] initWithNibName:@"SingleContactDetails" bundle:nil];
    self.deligate = singleContactDetails;
    Course *course= self.contactDetails[indexPath.row];
    [self presentViewController:singleContactDetails animated:YES completion:nil];
    [self.deligate passContactDetails:course.company andEmail:course.emailAddresss andImage:course.image];

}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *myReturnSection=[NSString stringWithFormat:@"section %ld",(long)section];
    return myReturnSection;
}




@end
